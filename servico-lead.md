# Serviço de leads 


# Resumo


## Mensageria
* lead_command_bus (exchange) -> lead_command_queue (queue)
* lead_event_bus   (exchange) -> lead_event_queue (queue)

Os exhanges são do tipo x-delayed direct, ou seja funcionam baseados nas routing keys. As routing keys tem os mesmo nome dos eventos tratados pela aplicação.

## Eventos que esse serviço reage
 * PrecoConversaoCalculadoEvent
 * AguardandoUltimasConversoesGeradasEvent
 * ConversaoCompradaEvent
 * LeadNegativadaEvent

### PrecoConversaoCalculadoEvent 
Evento Lançado pelo serviço de preço após analisar o perfil da lead e definir um preço para a mesma. Nesse ponto o serviço de lead espera receber nesse evento o id da conversão em questão e o preço a ser definido. A responsabilidade do handler desse evento é apenas setar o preço no documento da conversão no mongo.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string
preco | preco calculado pelo servico de preco | numero

Em caso de sucesso o handler lança o evento **PrecoConversaoRegistradoEvent**

### AguardandoUltimasConversoesGeradasEvent
Lançado pelo serviço distribuidor de leads para obter desse serviço as últimas leads registradas no sistemas para dar continuidade processo de decisão do destino de alguma lead recém gerada. O sistema de leads espera que nesse evento o id da conversão que o sistema distribuidor está a processar no momento. A responsabilidade do handler aqui é buscar as ultimas 4 conversoes elegíveis para compra anteriores à conversão enviada pelo serviço distribuidor.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

O retorno dessas conversões se dá a partir do lançamento do evento **UltimasConversoesGeradasEcontradaEvent**

### ConversaoCompradaEvent 
Lançado pelo serviço de compra de lead após uma compra de lead é efetivada. O handler desse evento espera receber o id da conversao em questão. A função desse handler é apenas alterar o status da conversão no documento do mongo.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

### ConversaoNegativadaEvent
Lançado pelo serviço de negativação de lead após uma negativação ser aprovada. O handler desse evento também espera receber o id da conversão em questão. Como o handler de conversão esse handler tem a função de alterar o status da conversão no documento do mongo.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

Em caso de sucesso esse handler lança o evento de **StatusConversaoAlteradoEvent**

## Eventos Lançadoss
 * ConversaoRegistradaEvent
 * LeadCriadaEvent
 * PrecoConversaoRegistradoEvent
 * UltimasConversoesGeradasEcontradaEvent

 ### ConversaoRegistradaEvent
 Evento lançado após uma nova conversao ter sido inserida com sucesso na base do mongo.Esse evento contem todas as informações da lead recem inserida.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao inserida | string
nome | nome da lead | string
email | email da lead | string
telefone | telefone da lead | string
cidade | cidade da lead | string
produto | produto da lead | string
subproduto | subproduto da lead | string
status | status atual da lead | string
timestamp | horario da criacao da lead | numero
ip | ip da lead que enviou | string
preco | preco da lead | numero
dados_demograficos | objeto | string
informacoes_adicionais | objeto | string

### LeadCriadaEvent
Evento Lançado quando é a primeira conversão de uma lead no sistema. Esse evento contem toda as informações da lead, incluindo as suas conversões.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
email | email da lead | string 