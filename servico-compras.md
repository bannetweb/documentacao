# Serviço de compra de leads

# Resumo

## Mensageria
* compra_leads_service (exchange) -> compra_leads_event_bus_queue (queue)

Os exhanges sao do tipo x-delayed direct, ou seja funcionam baseados nas routing keys.As routing keys tem os mesmo nome dos eventos tratados pela aplicacao.

## Eventos que esse serviço reage
 * SolicitadoCompraConversaoEvent
 * StatusConversaoAlteradoEvent
 * AplicarMultaEvent
 * RecargaSuccessEven

### SolicitadoCompraConversaoEvent 
E lançado pelo "serviço de distribuição" ou POST da loja, validado se o comprador existe, se a lead ja esta sendo comprada 
e se o comprador tem saldo para compra 

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
idUsuario | id do usuario que vai comprar | string
conversao | conversao com todos os dados | objeto

* Em caso de sucesso o handler lança o evento **SolicitadoAlteracaoStatusParaConversaoCompradaEvent**
* Em caso de saldo insuficiente o handler lança o evento **SaldoInsuficienteCompraConversaoEvent**

```JSON
   {
     “titulo”: “JSON x XML”,
     “resumo”: “o duelo de dois modelos de representação de informações”,
     “ano”: 2019,
     “genero”: [“aventura”, “ação”, “ficção”] 
    },
   {
     “titulo”: “JSON James”,
     “resumo”: “a história de uma lenda do velho oeste”,
     “ano”: 2019,
     “genero”: [“western”]  
    }
```

### StatusConversaoAlteradoEvent
Ainda será descrito...

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

O retorno dessas conversoes se dá a partir do lanÃ§amento do evento **Ainda será descrito**

### ConversaoCompradaEvent 
Lançaado pelo serviço de compra de lead apos uma compra de lead não efetivada. O handler desse evento espera receber o id da conversão em questao. A função desse handler é apenas alterar o status da conversão no documento do mongo.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

### ConversaoNegativadaEvent
Lançado pelo serviço de negativação de lead apos uma negativação ser aprovada. O handler desse evento também espera receber o id da conversão em questao. Como o handler de conversão esse handler tem a função de alterar o status da conversão no documento do mongo.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao que teve seu preco calculado | string

Em caso de sucesso esse handler lanÃ§a o evento de **StatusConversaoAlteradoEvent**

## Eventos Lançados
 * ConversaoRegistradaEvent
 * LeadCriadaEvent
 * PrecoConversaoRegistradoEvent
 * UltimasConversoesGeradasEcontradaEvent

 ### ConversaoRegistradaEvent
 Evento lançado após uma nova conversão ter sido inserida com sucesso na base do mongo. Esse evento contem todas as informações da lead recem inserida.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
id | id da conversao inserida | string
nome | nome da lead | string
email | email da lead | string
telefone | telefone da lead | string
cidade | cidade da lead | string
produto | produto da lead | string
subproduto | subproduto da lead | string
status | status atual da lead | string
timestamp | horario da criacao da lead | numero
ip | ip da lead que enviou | string
preco | preco da lead | numero
dados_demograficos | objeto | string
informacoes_adicionais | objeto | string

### LeadCriadaEvent
Evento Lançado quando à primeira conversao de uma lead no sistema. Esse evento contem toda as informações da lead, incluindo as suas conversões.

Atributo esperado | Descricao | Tipo
------------ | ------------- | ----------
email | email da lead | string 